---
type: docs
---

This page contains Arch Linux monthly reports.

An RSS feed is available [here](https://monthly-reports.archlinux.page/index.xml).

Git repository: https://gitlab.archlinux.org/archlinux/monthly-reports
